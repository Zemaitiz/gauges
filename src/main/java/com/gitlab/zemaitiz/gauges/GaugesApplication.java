package com.gitlab.zemaitiz.gauges;

import com.gitlab.zemaitiz.gauges.model.Gauge;
import com.gitlab.zemaitiz.gauges.repository.GaugeRepository;
import com.gitlab.zemaitiz.gauges.services.GroupReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaugesApplication {


    public static void main(String[] args) {
        SpringApplication.run(GaugesApplication.class, args);
    }
}
