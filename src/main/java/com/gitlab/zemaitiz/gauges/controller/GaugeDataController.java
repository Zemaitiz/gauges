package com.gitlab.zemaitiz.gauges.controller;

import com.gitlab.zemaitiz.gauges.model.GaugeData;
import com.gitlab.zemaitiz.gauges.model.GaugeDataDTO;
import com.gitlab.zemaitiz.gauges.services.GaugeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/gaugedata")
public class GaugeDataController
        //extends ApiRestController
{

    @Autowired
    private GaugeDataService gaugeDataService;




    //READ by code and date
@GetMapping("/codedate")
    public GaugeDataDTO findGaugeDataByCodeAndDate(@RequestParam String code,@RequestParam int year,@RequestParam int month) {
        return gaugeDataService.findGaugeDataByCodeAndDate(code, year, month);
    }

    //read all
    @GetMapping("/all")
    public List<GaugeData> findAll() {
        return gaugeDataService.findAll();
    }

    //read all by Gauge id

    @GetMapping("/{gaugeId}")
    public List<GaugeData> findAllByGaugeId(@PathVariable Long gaugeId) {
        return gaugeDataService.findAllByGaugeId(gaugeId);
    }
// Read all by gauge code
    @GetMapping()
    public List<GaugeData> findAllByGaugeId(@RequestParam String code) {
        return gaugeDataService.findAllByGaugeCode(code);
    }

    //DELETE by id
    @DeleteMapping("/{gaugeDataId}")
    public void deleteGaugeDataById(@PathVariable Long gaugeDataId) {
        gaugeDataService.deleteGaugeDataById(gaugeDataId);
    }


    //CREATE
    @PostMapping("/new")
    public void addGaugeData(@RequestBody GaugeData gaugeData) {
        gaugeDataService.addGaugeData(gaugeData);
    }
}

