package com.gitlab.zemaitiz.gauges.controller;

import com.gitlab.zemaitiz.gauges.model.Gauge;
import com.gitlab.zemaitiz.gauges.services.GaugeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/gauge")
public class GaugeController
        //extends ApiRestController
{

    @Autowired
    private GaugeService gaugeService;

    //CREATE Gauge
    @PostMapping("/addgauge")
    public void addGauge() {
    }


    //READ on by id
    @GetMapping("/findbyid/{gaugeId}")
    public Optional<Gauge> getGaugeById(@PathVariable Long gaugeId) {
        return gaugeService.findGaugeById(gaugeId);
    }

    //READ all
    @GetMapping("/gaugelist")
    public List<Gauge> listAllGauges() {
        return gaugeService.findAllGauges();
    }

    //UPDATE by id
    @PutMapping("/updategauge/{gaugeId}")
    public void updateGauge(@PathVariable Long gaugeId, @RequestBody Gauge gauge) {
      gaugeService.updateGauge(gaugeId, gauge);
    }

    //DELETE by id
    @DeleteMapping("/deletegauge/{gaugeId}")
    public void deleteById(@PathVariable Long gaugeId) {
        gaugeService.deleteGaugeById(gaugeId);
    }

}
