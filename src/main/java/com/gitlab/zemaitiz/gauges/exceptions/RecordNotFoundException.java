package com.gitlab.zemaitiz.gauges.exceptions;

public class RecordNotFoundException extends Exception {

    public RecordNotFoundException(String message) {
        super(message);
    }


}
