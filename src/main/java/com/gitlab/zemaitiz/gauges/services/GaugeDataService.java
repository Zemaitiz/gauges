package com.gitlab.zemaitiz.gauges.services;

import com.gitlab.zemaitiz.gauges.model.*;
import com.gitlab.zemaitiz.gauges.repository.GaugeDataRepository;
import com.gitlab.zemaitiz.gauges.repository.GaugeRepository;
import com.gitlab.zemaitiz.gauges.repository.GroupReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GaugeDataService {

    @Autowired
    private GaugeDataRepository gaugeDataRepository;

    @Autowired
    private GaugeRepository gaugeRepository;

    @Autowired
    private GroupReadingRepository groupReadingRepository;



    public void addGaugeData(GaugeData gaugeData) {
        gaugeDataRepository.save(gaugeData);
    }

    //READ

    public GaugeDataDTO findGaugeDataByCodeAndDate(String code, int year, int month) {
List<GaugeData> gaugeData = gaugeDataRepository.findAllByGaugeCode(code);
GaugeDataDTO gaugeDataDTO = new GaugeDataDTO();
        for (GaugeData gaugeDatum : gaugeData) {
            if (gaugeDatum.getReadingDate().getYear() == year && gaugeDatum.getReadingDate().getMonthValue() == month)
gaugeDataDTO.setId(gaugeDatum.getId());
            gaugeDataDTO.setGaugeCode(gaugeDatum.getGauge().getCode());
            gaugeDataDTO.setCurrentValue(gaugeDatum.getCurrentValue());
            gaugeDataDTO.setReadingDate(gaugeDatum.getReadingDate());
        }
return gaugeDataDTO;
    }

    public List<GaugeData> findAll() {
        return (List<GaugeData>) gaugeDataRepository.findAll();
    }

    public List<GaugeData> findAllByGaugeId(Long gaugeId) {
        return gaugeDataRepository.findAllByGaugeId(gaugeId);
    }

    public List<GaugeData> findAllByGaugeCode(String code) {
        return gaugeDataRepository.findAllByGaugeCode(code);
    }


    //UPDATE

    //TODO sutvarkyti
    public void updateGaugeDataById(Long gaugeDataId, GaugeDataDTO dto) {
        Optional<GaugeData> gaugeDataToUpdate = gaugeDataRepository.findById(dto.getId());
        gaugeDataToUpdate.get().setGauge(gaugeRepository.findByCode(dto.getGaugeCode()));
        gaugeDataToUpdate.get().setCurrentValue(dto.getCurrentValue());
        gaugeDataRepository.save(gaugeDataToUpdate.get());
    }

    //DELETE
    public void deleteGaugeDataById(Long gaugeDataId) {
        Optional<GaugeData> gaugeDataToDelete = gaugeDataRepository.findById(gaugeDataId);
        gaugeDataRepository.delete(gaugeDataToDelete.get());
    }

}
