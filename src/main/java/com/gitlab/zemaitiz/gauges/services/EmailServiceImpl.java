package com.gitlab.zemaitiz.gauges.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String message) {
        SimpleMailMessage msg = new SimpleMailMessage();
        String mailTitle = message.substring(0,7);


        msg.setTo("edvinas.prokofijovas@gmail.com");
        msg.setSubject(mailTitle);
        msg.setText(message);

        javaMailSender.send(msg);
    }

}
