package com.gitlab.zemaitiz.gauges.services;

import com.gitlab.zemaitiz.gauges.model.*;
import com.gitlab.zemaitiz.gauges.repository.GaugeRepository;
import com.gitlab.zemaitiz.gauges.repository.GroupReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class GroupReadingService {
    @Autowired
    private GroupReadingRepository groupReadingRepository;

    @Autowired
    private GaugeRepository gaugeRepository;

// Populate db with pseudo-random group Readings

    public void populateWithPseudoRandom() {


        BigDecimal otherServices = new BigDecimal(Math.random());
        int initialKh = 1;
        int initialKc = 1;
        int initialBh = 1;
        int initialBc = 1;
        int initialEd = 1;
        int initialEn = 1;


        for (int i = 0; i < 20; i++) {
            LocalDate currentDate = LocalDate.of(2020, 01, 25);
            GroupReading groupReading = new GroupReading();
            Set<GaugeData> gaugeDataSet = groupReading.getGaugeDataSet();
            LocalDate incrementedDate = currentDate.plusMonths(i);
            groupReading.setReadingDate(incrementedDate);

            initialKh += 1;
            initialKc += 1;
            initialBh += 1;
            initialBc += 1;
            initialEd += 1;
            initialEn += 1;

            GaugeData khData = new GaugeData(gaugeRepository.findById(1L).get(), groupReading, initialKh, incrementedDate);
            GaugeData kcData = new GaugeData(gaugeRepository.findById(2L).get(), groupReading, initialKc, incrementedDate);
            GaugeData bhData = new GaugeData(gaugeRepository.findById(3L).get(), groupReading, initialBh, incrementedDate);
            GaugeData bcData = new GaugeData(gaugeRepository.findById(4L).get(), groupReading, initialBc, incrementedDate);
            GaugeData edData = new GaugeData(gaugeRepository.findById(5L).get(), groupReading, initialEd, incrementedDate);
            GaugeData enData = new GaugeData(gaugeRepository.findById(6L).get(), groupReading, initialEn, incrementedDate);

            gaugeDataSet.add(khData);
            gaugeDataSet.add(kcData);
            gaugeDataSet.add(bhData);
            gaugeDataSet.add(bcData);
            gaugeDataSet.add(edData);
            gaugeDataSet.add(enData);
            groupReading.setGaugeDataSet(gaugeDataSet);
            groupReading.setOtherServices(otherServices);
            groupReadingRepository.save(groupReading);
        }
    }


    //CREATE New group reading
    public void saveGroupReading(GroupReadingDataDTO dto) {
        GroupReading groupReading = new GroupReading();
        Set<GaugeData> gaugeDataSet = groupReading.getGaugeDataSet();
        //groupReading.setReadingDate(dto.getCurrentDate());
        LocalDate currentDate = LocalDate.now();
        currentDate.format(DateTimeFormatter.ISO_DATE);
        BigDecimal otherServices = dto.getOtherServices();


        GaugeData khData = new GaugeData(gaugeRepository.findById(1L).get(), groupReading, dto.getkHvalue(), currentDate);
        GaugeData kcData = new GaugeData(gaugeRepository.findById(2L).get(), groupReading, dto.getkCvalue(), currentDate);
        GaugeData bhData = new GaugeData(gaugeRepository.findById(3L).get(), groupReading, dto.getbHvalue(), currentDate);
        GaugeData bcData = new GaugeData(gaugeRepository.findById(4L).get(), groupReading, dto.getbCvalue(), currentDate);
        GaugeData edData = new GaugeData(gaugeRepository.findById(5L).get(), groupReading, dto.geteDvalue(), currentDate);
        GaugeData enData = new GaugeData(gaugeRepository.findById(6L).get(), groupReading, dto.geteNvalue(), currentDate);

        gaugeDataSet.add(khData);
        gaugeDataSet.add(kcData);
        gaugeDataSet.add(bhData);
        gaugeDataSet.add(bcData);
        gaugeDataSet.add(edData);
        gaugeDataSet.add(enData);
        groupReading.setGaugeDataSet(gaugeDataSet);
        groupReading.setOtherServices(otherServices);
        groupReadingRepository.save(groupReading);
        gaugeDataSet.forEach(gaugeData -> System.out.println(gaugeData.getGauge().getCode()));
        gaugeDataSet.forEach(gaugeData -> System.out.println(gaugeData.toString()));
    }


    //READ all datasets of all gauge readings

    public List<Set<GaugeData>> findAllGroupReadingsGaugeDataSets() {
        List<Set<GaugeData>> allGaugeDataSetList = new ArrayList<>();
        groupReadingRepository.findAll().forEach(groupReading -> {
                    allGaugeDataSetList.add(groupReading.getGaugeDataSet());
                }
        );
        return allGaugeDataSetList;
    }

    //READ all GaugeData by date (month)

    public Set<GaugeData> findGroupReadingGaugeDataSetByDate(int year, int month) {
        Iterable<GroupReading> groupReadings = groupReadingRepository.findAll();
        Set<GaugeData> gd = new LinkedHashSet<>();
        for (GroupReading g : groupReadings) {
            if (g.getReadingDate().getYear() == year && g.getReadingDate().getMonth().getValue() == month) {
                gd = g.getGaugeDataSet();
            }

        }
        return gd;
    }


    //CREATE a missing GaugeData reading and assign it to GroupReading's set
    public void updateGroupReading(Long groupReadingId, List<GaugeDataDTO> gDTO) {
        GroupReading groupReadingToUpdate = groupReadingRepository.findById(groupReadingId).get();
        Set<GaugeData> setToUpdate = groupReadingToUpdate.getGaugeDataSet();
        LocalDate currentDate = LocalDate.now();
        for (GaugeDataDTO gdt : gDTO) {
            for (GaugeData oldGd : setToUpdate) {
                if (gdt.getGaugeCode().equalsIgnoreCase(oldGd.getGauge().getCode())) {
                    oldGd.setCurrentValue(gdt.getCurrentValue());
                    oldGd.setReadingDate(currentDate);
                }
            }
            groupReadingRepository.save(groupReadingToUpdate);
        }

    }

    //READ all GaugeData by group reading id

    public Set<GaugeData> findAllByGroupReadingId(Long groupReadingId) {

        return groupReadingRepository.findById(groupReadingId).get().getGaugeDataSet();
    }


    //READ all group readings

    public Iterable<GroupReading> findAllGroupReadings() {
        return groupReadingRepository.findAll();
    }

    //READ GroupReading by id

    public GroupReading findById(Long groupReadingId) {
        return groupReadingRepository.findById(groupReadingId).get();
    }


    //Find incomplete groupReadings

    public List<GroupReading> findIncompleteGroupReadings() {
        List<GroupReading> incompleteReadings = new ArrayList<>();

        for (GroupReading groupReading : groupReadingRepository.findAll())
            for (GaugeData gaugeData : groupReading.getGaugeDataSet()) {
                if (gaugeData.getCurrentValue() == 0) {
                    incompleteReadings.add(groupReading);
                }
            }
        return incompleteReadings;
    }


    //DELETE by ID

    public void deleteGroupReadingById(Long groupReadingId) {
        groupReadingRepository.deleteById(groupReadingId);
    }
}






