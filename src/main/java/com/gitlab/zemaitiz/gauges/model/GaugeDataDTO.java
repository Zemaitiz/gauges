package com.gitlab.zemaitiz.gauges.model;

import java.time.LocalDate;

public class GaugeDataDTO {



    private Long id;

    private String gaugeCode;

    private int currentValue;

    private LocalDate readingDate;


    public String getGaugeCode() {
        return gaugeCode;
    }

    public void setGaugeCode(String gaugeCode) {
        this.gaugeCode = gaugeCode;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public LocalDate getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(LocalDate readingDate) {
        this.readingDate = readingDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
