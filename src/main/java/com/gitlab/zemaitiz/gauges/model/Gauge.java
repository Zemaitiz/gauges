package com.gitlab.zemaitiz.gauges.model;
import lombok.Data;
import org.springframework.stereotype.Service;


import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "gauge")
public class Gauge implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private String code;
    private String typeDescription;
    private String typeDescriptionLt;
    private String units;
}
