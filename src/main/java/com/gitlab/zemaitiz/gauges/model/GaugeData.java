package com.gitlab.zemaitiz.gauges.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "gauge_data")
public class GaugeData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gauge_code", referencedColumnName = "code")
    private Gauge gauge;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_reading_id")
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private GroupReading groupReading;

    private int currentValue;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate readingDate = LocalDate.now();

    public GaugeData() {
    }

    public GaugeData(Gauge gauge, GroupReading groupReading, int currentValue, LocalDate readingDate) {
        this.gauge = gauge;
        this.groupReading = groupReading;
        this.currentValue = currentValue;
        this.readingDate = readingDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Gauge getGauge() {
        return gauge;
    }

    public void setGauge(Gauge gauge) {
        this.gauge = gauge;
    }

    public GroupReading getGroupReading() {
        return groupReading;
    }

    public void setGroupReading(GroupReading groupReading) {
        this.groupReading = groupReading;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public LocalDate getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(LocalDate readingDate) {
        this.readingDate = readingDate;
    }

    @Override
    public String toString() {
        return
                 gauge.getTypeDescriptionLt() + ": " + currentValue + " " + gauge.getUnits();
    }
}
