package com.gitlab.zemaitiz.gauges.model;


import org.springframework.format.annotation.DateTimeFormat;


import java.math.BigDecimal;
import java.time.LocalDate;


public class GroupReadingDataDTO {
   /* @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate currentDate;*/
    private int kHvalue;
    private int kCvalue;
    private int bHvalue;
    private int bCvalue;
    private int eDvalue;
    private int eNvalue;

    private BigDecimal otherServices;

    /*public LocalDate getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(LocalDate currentDate) {
        this.currentDate = currentDate;
    }*/

    public int getkHvalue() {
        return kHvalue;
    }

    public void setkHvalue(int kHvalue) {
        this.kHvalue = kHvalue;
    }

    public int getkCvalue() {
        return kCvalue;
    }

    public void setkCvalue(int kitchenColdWaterValue) {
        this.kCvalue = kitchenColdWaterValue;
    }

    public int getbHvalue() {
        return bHvalue;
    }

    public void setbHvalue(int bHvalue) {
        this.bHvalue = bHvalue;
    }

    public int getbCvalue() {
        return bCvalue;
    }

    public void setbCvalue(int bCvalue) {
        this.bCvalue = bCvalue;
    }

    public int geteDvalue() {
        return eDvalue;
    }

    public void seteDvalue(int eDvalue) {
        this.eDvalue = eDvalue;
    }

    public int geteNvalue() {
        return eNvalue;
    }

    public void seteNvalue(int eNvalue) {
        this.eNvalue = eNvalue;
    }

    public BigDecimal getOtherServices() {
        return otherServices;
    }

    public void setOtherServices(BigDecimal otherServices) {
        this.otherServices = otherServices;
    }
}
