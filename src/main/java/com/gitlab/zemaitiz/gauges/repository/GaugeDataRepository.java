package com.gitlab.zemaitiz.gauges.repository;

import com.gitlab.zemaitiz.gauges.model.Gauge;
import com.gitlab.zemaitiz.gauges.model.GaugeData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
@Repository
public interface GaugeDataRepository extends CrudRepository<GaugeData, Long> {

    List<GaugeData> findAllByGaugeId(Long gaugeId);
    List<GaugeData> findAllByGaugeCode(String code);
}
