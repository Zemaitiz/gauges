package com.gitlab.zemaitiz.gauges.repository;

import com.gitlab.zemaitiz.gauges.model.GroupReading;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupReadingRepository extends CrudRepository<GroupReading,Long> {
}
