package com.gitlab.zemaitiz.gauges.repository;

import com.gitlab.zemaitiz.gauges.model.Gauge;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GaugeRepository extends CrudRepository<Gauge, Long> {
    Gauge findByCode(String code);
}
