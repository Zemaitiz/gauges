INSERT INTO gauge(code, type_description, units) VALUES('KH', 'Kitchen hot water', 'm^3');
INSERT INTO gauge(code, type_description, units) VALUES('KC', 'Kitchen cold water', 'm^3');
INSERT INTO gauge(code, type_description, units) VALUES('BH', 'Bathroom hot water', 'm^3');
INSERT INTO gauge(code, type_description, units) VALUES('BC', 'Bathroom cold water', 'm^3');
INSERT INTO gauge(code, type_description, units) VALUES('ED', 'Daytime energy', 'm^3');
INSERT INTO gauge(code, type_description, units) VALUES('EN', 'Nighttime energy', 'm^3');
